import 'dart:io';

import 'package:dart_application_1/dart_application_1.dart'
    as dart_application_1;

void main(List<String> arguments) {
  print("Select the choice you want to perform :");
  print("1. ADD");
  print("2. SUBTRACT");
  print("3. MULTIPLY");
  print("4. DIVIDE");
  print("5. EXIT");

  print("Choice you want to enter :");
  var operator = stdin.readLineSync();

  switch (operator) {
    case "1":
      {
        print("Enter the value for x :");
        var n1 = stdin.readLineSync();
        var x = int.parse(n1!);
        print("Enter the value for y :");
        var n2 = stdin.readLineSync();
        int y = int.parse(n2!);
        print("Sum of the two number is :");
        print(x + y);
      }
      break;

    case "2":
      {
        print("Enter the value for x :");
        var n1 = stdin.readLineSync();
        var x = int.parse(n1!);
        print("Enter the value for y :");
        var n2 = stdin.readLineSync();
        int y = int.parse(n2!);
        print("Difference of the two number is :");
        print(x - y);
      }
      break;
    case "3":
      {
        print("Enter the value for x :");
        var n1 = stdin.readLineSync();
        var x = int.parse(n1!);
        print("Enter the value for y :");
        var n2 = stdin.readLineSync();
        int y = int.parse(n2!);
        print("Multiply of the two number is :");
        print(x * y);
      }
      break;
    case "4":
      {
        print("Enter the value for x :");
        var n1 = stdin.readLineSync();
        var x = int.parse(n1!);
        print("Enter the value for y :");
        var n2 = stdin.readLineSync();
        int y = int.parse(n2!);
        print("Quotient of the two number is :");
        print(x / y);
      }
      break;
    case "5":
      break;
  }
}
